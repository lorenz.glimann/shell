

Command
	= symbol:String _ args:( String _)* '=' _ cmd:( SubCommand _)+ {
    	return {
        	"type": "assignment",
        	"symbol": symbol,
            "args": args.map(x=>x[0]),
            "data": cmd.map(x=>x[0])
        }
    }
    / symbol:String _ cmd:( SubCommand _)* {
    	return {
        	"type": "command",
            "symbol": symbol,
            "data": cmd.map(x=>x[0])
        }
    }
    / ""
    //{ return cmd.map(x => x[0]) }

SubCommand
	= '(' symbol:String _ cmd:( SubCommand _)* ')' {
    	return {
        	"type": "command",
            "symbol": symbol,
            "data": cmd.map(x=>x[0])
        }
    }
	/ s:String { return s }

String = s:[a-zA-Z0-9\*\+|]+ { return s.join(""); }

_ "whitespace"
	= [ \t\r]*
