# simple lisplike shell
## How to use
1. install system dependencies
2. clone repo
3. run `yarn` oder `npm install` from within the repo to install js dependencies
4. run `yarn run start`/`npm start` to compile the grammar and run the interpreter OR
5. just run the interpreter with `node index.js`

System Dependencies:
- git
- node js
- yarn/npm

## How it works
- all programs are expressions
- all expressions are withing round brackets () excepting:
    - there are implicit round brackets () around the outer expression
- expressions get parsed into an abstrace syntax tree (AST)
    - currently 2 types of nodes (expression, assignment) and 1 atom (string)
- the AST gets interpreted by a recursive function

## What works
- expressions/return values/outputs
    - `> echo hi` // => hi
- nested expressions
    - `> echo (echo hi)` // => hi
- `> x = echo hi` // global symbol binding (aka setting variables); lazy evaluated
    - `> x` // calling nested expressions
- piping, after a fashion; currently bash does the piping for us ;)
- extending the language by writing js functions

## What doesn't (yet)
- piping inside the language (currently bash does it)
- local variables/context/`this`
- symbols with arguments (aka functions with arguments)
    - `> f x = echo (x)`
    - `> + x y = echo (x)+(y) | bc`
- extending the language with macros (functions on the raw ast)
