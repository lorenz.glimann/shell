const chalk = require('chalk');
const execa = require("execa");

symbols = {
    info: async function (symbol, args) {
        return `hallo lieber erdling. ich bin eine seltsame Kreuzung aus bash, lisp und haskell. Noch kann man mich keine Missgeburt taufen, denn es ist ja kaum der Kopf noch nicht draußen...`;
    },
    run: async function (symbol, args) {
        let result = "";
        try {
            result = await execa.shell(symbol + " " + args.join(" "));
        } catch (e) {
            return chalk.red(e);
        }
        return result.stdout;
    },
    debug: async function (symbol, args) {
        console.log(chalk.green(JSON.stringify(args)));
        console.log(symbols.f.toString());
    }
};

function symbolFactory(symbol, args) {
    symbols[symbol] = function () {

    };
}

/**
 * returns a function that can be called with arguments
 * @param symbol
 */
function lookup(symbol) {
    if (symbols.hasOwnProperty(symbol)) {
        return symbols[symbol];
    }
    return symbols.run;
}

async function evaluate(command) {
    if (typeof command === "string") {
        return command;
    }
    // command is an object
    switch (command.type) {
        case "command":
            // first evaluate all children nodes
            for (let i = 0; i < command.data.length; i++) {
                command.data[i] = await evaluate((command.data[i]));
            }
            return await lookup(command.symbol)(command.symbol, command.data);
            break;
        case "assignment":
            symbols[command.symbol] = async (symbol, args) => { // args not used
                // for (let i = 0; i < command.data.length; i++) {
                //     command.data[i] = await evaluate((command.data[i]));
                // }
                return evaluate({
                    "type": "command",
                    "symbol": command.data[0],
                    "data": command.data.slice(1)
                });
            };
            //throw "Error: assignment not implemented";
            break;
    }
}
module.exports = evaluate;
