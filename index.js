let grammar = require("./grammar");
let evaluate = require("./evaluate");

var readline = require('readline');
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});
rl.setPrompt("> ");
rl.prompt();

rl.on('line', function(line){
    evaluate(grammar.parse(line)).then((result) => {
        console.log(result);
        rl.prompt();
    });
});
